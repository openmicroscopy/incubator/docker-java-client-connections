# Templated Dockerfile for testing OMERO Java clients in different environments

## Build
Set the `PARENT` build-arg to the base image including optional tag you want to use such as `fedora`, `centos:7`, `ubuntu`, `debian:unstable`. E.g.

    docker build -t test --build-arg=PARENT=fedora:latest .

## Run
Pass arguments to the [`minimal-omero-client`](https://github.com/ome/minimal-omero-client). E.g.

    docker run --rm test --omero.host=omero.example.org --omero.user=root --omero.pass=omero \
        --IceSSL.Ciphers='(DH_anon.*AES)' --IceSSL.Trace.Security=1
