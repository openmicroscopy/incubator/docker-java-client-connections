#!/bin/sh
set -eu
date
cat /etc/issue.net
java -version
openssl version

export JAVA_HOME=/usr
/minimal-omero-client/build/distributions/minimal-omero-client-5.5.0/bin/minimal-omero-client "$@"
