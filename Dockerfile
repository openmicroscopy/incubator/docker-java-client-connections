ARG PARENT
FROM $PARENT
LABEL maintiner=ome-devel@lists.openmicroscopy.org.uk

ADD install.sh /install.sh
ARG PARENT
RUN ./install.sh $PARENT

ADD entrypoint.sh /bin/entrypoint.sh
ENTRYPOINT ["/bin/entrypoint.sh"]
