#!/bin/sh
# Should be POSIX compatible

set -eu
set -x
case $1 in
    centos*|rhel*)
        yum install -y -q java-11-openjdk-devel || yum install -y -q java-openjdk-devel
        yum install -y -q java-devel git openssl
    ;;
    fedora*)
        dnf install -y -q java-11-openjdk-devel || yum install -y -q java-openjdk-devel
        dnf install -y -q java-devel git openssl
    ;;
    ubuntu*|debian*)
        apt-get update
        apt-get install -y -q openjdk-11-jdk || apt-get install -y -q openjdk-8-jdk
        apt-get install -y -q git openssl
    ;;
    *)
        echo "Unknown parent: $1"
        exit 1
    ;;
esac

export JAVA_HOME=/usr

cd /
# Requires
# - https://github.com/ome/minimal-omero-client/pull/42
# - https://github.com/ome/minimal-omero-client/pull/43
# - https://github.com/ome/minimal-omero-client/pull/44
git clone -b pr42+pr43+pr44 --depth 1 https://github.com/manics/minimal-omero-client
cd minimal-omero-client
./gradlew --no-daemon build
cd build/distributions/
tar xf minimal-omero-client-5.5.0.tar
